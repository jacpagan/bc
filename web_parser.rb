#!/usr/bin/env ruby
#Parses bitcoin website 
require 'rubygems'
require "net/http"
require "uri"
require 'json'
require 'yaml'
require 'net/smtp'
require 'pony'
require 'pp'
=begin
#Get url 
uri = URI.parse("http://api.bitcoincharts.com/v1/weighted_prices.json")
http = Net::HTTP.new(uri.host, uri.port)
res = http.request_head(uri.path)

=end
#http://www.exchangerates.org.uk/
#http://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22USDEUR%22,%20%22USDJPY%22,%20%22USDBGN%22,%20%22USDCZK%22,%20%22USDDKK%22,%20%22USDGBP%22,%20%22USDHUF%22,%20%22USDLTL%22,%20%22USDLVL%22,%20%22USDPLN%22,%20%22USDRON%22,%20%22USDSEK%22,%20%22USDCHF%22,%20%22USDNOK%22,%20%22USDHRK%22,%20%22USDRUB%22,%20%22USDTRY%22,%20%22USDAUD%22,%20%22USDBRL%22,%20%22USDCAD%22,%20%22USDCNY%22,%20%22USDHKD%22,%20%22USDIDR%22,%20%22USDILS%22,%20%22USDINR%22,%20%22USDKRW%22,%20%22USDMXN%22,%20%22USDMYR%22,%20%22USDNZD%22,%20%22USDPHP%22,%20%22USDSGD%22,%20%22USDTHB%22,%20%22USDZAR%22,%20%22USDISK%22%29&env=store://datatables.org/alltableswithkeys
#http://currency-api.appspot.com/documentation
#http://quotes.instaforex.com/get_quotes.php
#https://spreadsheets.google.com/feeds/list/0Av2v4lMxiJ1AdE9laEZJdzhmMzdmcW90VWNfUTYtM2c/2/public/basic?alt=json
#http://openexchangerates.org/api/latest.json?app_id=63460294e9c44bb28dafbea386c4eca1
#http://themoneyconverter.com/rss-feed/USD/rss.xml
#https://github.com/mhs/world-currencies/blob/master/currencies.json

# Takes currencies as input  i.e  BTC USD GBP CNY JPY  

# Prints out a exchange rate chart 
# Tri-currency arbitrage 
=begin
        BTC    USD     GBP      CNY       JPY  
        
BTC      1
USD             1
GBP                     1     
CNY                               1
JPY                                         1

=end
@bitCoinChartLink = "http://api.bitcoincharts.com/v1/weighted_prices.json" #JPY & BTC & DKK not working 
@forexCoinChartLink = "http://openexchangerates.org/api/latest.json?app_id=63460294e9c44bb28dafbea386c4eca1"
#@toAddress = "jvpagan@msn.com"
@toAddress = "ryanalan.moore@gmail.com"
#@toAddress = "jpagan@enphaseenergy.com"
@currency_array = ["USD","GBP","EUR","CNY","JPY","AUD","CAD","MXN","RUB","NOK","BRL","INR"]


class Coin
  def initialize()

#@currency = 3 letter acronym for the country's currency in the market
#twentyFourHour, sevenD, thrityD are weighted priced that are calculated for the last 24 hours, 7 days and 30 days. If there are no trades during intervals, no value will be return. Must handle. 
    @currency = '' 
    @bit =''
    @forex = '' 
    @thirtyD = ''
    @timestamp = ''

  end

  def setTimestamp(time)
    @timestamp = time
  end

  def getTimestamp()
    return @timestamp
  end
  
  def setCurrency(currency)
  	@currency = currency
  end

  def getCurrency()
  	return @currency
  end

  def setForex(num)
  	@forex = num
  end

  def getForex()
  	return @forex
  end

  def setThirty(num)
  	@thirtyD = num
  end

  def getThirty()
  	return @thirtyD
  end

  def setBit(num)
  	@bit = num
  end

  def getBit()
  	return @bit
  end
  
  def printCoin(usdCoin)
    puts "#{getForex.to_f}"
  end
end

#If it hasn't changed more than %1 . 

class Email
  def initialize()
    @mailType = 'smtp'
    @fromAddress = 'jpagan@enphaseenergy.com'
    @options = {
      :address => 'smtp.gmail.com',
      :port => 587,
      :user_name => 'jpagan@enphaseenergy.com',
      :password => '',
      :domain => 'enphaseenergy.com'
    }
  end

  public
  # Sends an email
  #
  # Params:
  # * toList = The address or addresses to send the email to (if more than one, must be a list type)
  # * sub = Email subject
  # * message = The body of the message
  # * attachments = optional attachments (needs to be complete paths, including file name in the form of a list)
  def send(toList, sub, message)
    puts "Sending email to "
    puts toList.inspect

    Pony.mail(:to => toList, :via => :smtp, :via_options => @options, :from => @fromAddress, :subject => sub, :html_body => message)
  end
end



def getYaml(json)
  #example command: cat /opt/emu/httpd/rhtdocs/config/locales/en.yml
  #if(RUBY_VERSION >= "1.9.0")
  if(defined? YAML::ENGINE.yamler)
    YAML::ENGINE.yamler = 'syck'
  end
  yaml = YAML::parse(json)
  return yaml
end


def checkUrlResponse(link)
#This methods checks the url response code of the links to see if there is a possible error
  hash = {}
  url = URI.parse(link)
  begin
    res = Net::HTTP.get_response(url)
    return res
  rescue
  	"There was an error with the website"
  end
  return false
end



#generateCoin 
# output of generateCoin is sent 


#  bit =  (bitcoin.getTwentyFourHour.to_f / usdCoin.getTwentyFourHour.to_f).round(6)
  #puts "#{fx.getCurrency}\t|\t#{fx.getForex.to_f}\t|\t#{bit}\t|\t" 

#Email functionn to format the email 
def emailFormatAndSend(bitcoin_array, currency_array, fx_array)

  body = ""
  body <<"
    <table>
    <caption>#{Time.at(bitcoin_array.first.getTimestamp)}</caption>
    <tr>
    <th align='center'>currency</th>
    <th align='center'>rate</th>
    <th align='center'>J/USDBit</th>
    <th align='center'>BTC/USD</th>
    <th align='center'>(J/BTC)/(BTC/USD)</th>
    <th align='center'>(J/BTC)*(BTC/USD)</th>

    "
=begin
  currency_array.each do |currency|
    body <<"
    <td align='right'>#{currency}</td>"
  end
=end
  body <<"
  </tr>"
 
  usdCoin = Coin.new

  currency_array.each do |currency|
    fx_array.each do |fx| 
      bitcoin_array.each do |bitcoin|
        if(currency == bitcoin.getCurrency) && (currency == fx.getCurrency)
          if (currency == "USD")
           usdCoin = bitcoin
          end

          body << "<tr>
                <td align='right'>#{bitcoin.getCurrency}</td>
                <td align='right'>#{fx.getForex.to_f}</td>
                <td align='right'>#{bitcoin.getBit}</td>
                <td align='right'>#{usdCoin.getBit.to_f}</td>
                <td align='right'>#{(bitcoin.getBit.to_f / usdCoin.getBit.to_f).round(6)}</td>
                <td align='right'>#{((fx.getForex.to_f/bitcoin.getBit.to_f)*usdCoin.getBit.to_f).round(6)}</td> 

              </tr>" 
        end
      end
    end
  end
  mailer = Email.new()
  body << "</table>"
  mailer.send(@toAddress, bitcoin_array.first.getTimestamp, body)
end


# Then a function to send 

def generateCoinChart(link, rate)
  bitcoin_array = []

  res = checkUrlResponse(link)

  if(res != false)
    json = JSON.parse(res.body)
    json_pretty = JSON.pretty_generate json
    yaml = YAML::load(json_pretty)
    timestamp = yaml['timestamp']
  end 

  if(json != nil)

    if(rate)
      json = json["rates"]
     
      json.each do |key, value|
        coin = Coin.new
        coin.setCurrency(key)
        coin.setTimestamp(timestamp)
        coin.setForex('%.2f' % value.to_f)
        bitcoin_array.push(coin)
      end

    else
      json.each_pair do |key, value|

        if(key != "timestamp")
        	coin = Coin.new
        	coin.setCurrency(key)
          coin.setTimestamp(timestamp)
          coin.setBit('%.2f' % value["24h"].to_f)
          bitcoin_array.push(coin)
        end
      end
    end
  end
  return bitcoin_array
end
=begin
bitcoin_array has        
   coin.setCurrency(key)
   coin.setTimestamp(timestamp)
   coin.setTwentyFourHour('%.2f' % value.to_f)
=end
def printHeader
  puts "curr\t|\tRate\t|\tBTC\t\t|\tGBP\t|\tEUR\t|\tCNY\t|\tAUD\t|\tCAD\t|\tMXN\t|\tRUB\t|\tNOK\t|\tBRL\t|\tINR\t|"
end



def getForexCurrencyWithType(bitcoin_array, currency_array, fx_array, type)
  c = Coin.new
  currency_array.each do |currency|
      bitcoin_array.each do |bitcoin|
        fx_array.each do |fx| 
          if(currency == bitcoin.getCurrency) && (currency == fx.getCurrency)
            if (currency == type)
              c = fx
            end 
          end
        end
      end
  end
  return c
end
bitcoin_array = generateCoinChart(@bitCoinChartLink, false)
fx_coin_array = generateCoinChart(@forexCoinChartLink, true)
#printCoinByName(bitcoin_array, @currency_array, fx_coin_array)
emailFormatAndSend(bitcoin_array, @currency_array, fx_coin_array)
